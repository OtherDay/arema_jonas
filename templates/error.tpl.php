<?php

function page_title () {
    echo "Error";
}

function page_contents() {
   ?>
    <div class='jumbotron'>
        <h1>500 Error. My bad.</h1>
        <p class="lead"><?php error_message(); ?></p>
        <p>Refresh the page, go back, or try again later.</p>
    </div>
    <?php
}